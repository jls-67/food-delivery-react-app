import React from "react";
import styles from "./Notfound.module.css";

const NotFound = () => {
    return (
        <div className={styles.error_container}>
            <h1 className={styles.error_heading}>404</h1>
            <p className={styles.error_p}>The page you were looking for is not found.</p>
        </div>
    )
}

export default NotFound;