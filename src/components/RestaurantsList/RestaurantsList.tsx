import styles from "./RestaurantsList.module.css";
import restaurants from "../data/restaurants";
import RestauListItem from "./RestauListItem/RestauListItem";


const RestaurantsList = (props: any): JSX.Element => {
    const {selectedCategory} = props;

    return (
        <div className={styles.cards_container}>
            <span className={styles.cards_container_title}>Nearby restaurants</span>
                <ul className={styles.cards_list}>
            {
                restaurants.map((item: any) =>
                (item.category.includes(selectedCategory) || !selectedCategory ) && (
                <li>
                    <RestauListItem
                        key={item.id}
                        item={item}
                />
                </li>))
            }
            </ul>
        </div>
        )
    }

export default RestaurantsList;