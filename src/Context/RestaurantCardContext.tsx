import { useContext, createContext, ReactNode, useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import RestauListItem from "../components/RestaurantsList/RestauListItem/RestauListItem";
import RestaurantCart from "../components/Basket/Basket";


type RestaurantCartProviderProps = {
    children: ReactNode
}

type CartItem = {
    id: number
    quantity: number
}

type RestaurantCartContext = {
    cartQuantity: number,
    cartItems: CartItem[],
    getItemQuantity: (id: number) => number,
    increaseCartQuantity: (id: number) => void,
    decreaseCartQuantity: (id: number) => void,
    removeFromCart: (id: number) => void,
    setOpenBasket: (openBasket: boolean) => void,
    openBasket: boolean
}

const RestaurantCartContext = createContext({} as RestaurantCartContext)

export function useRestaurantCart() {
    return useContext(RestaurantCartContext)
}


export function RestaurantCartProvider({children}:
    RestaurantCartProviderProps ): JSX.Element {
        const [cartItems, setCartItems] = useState<CartItem[]>([]);
        const [openBasket, setOpenBasket] =useState(false);


        const cartQuantity = cartItems.reduce((quantity, item) => item.quantity + quantity, 0)


        function getItemQuantity (id:number) {
            return cartItems.find(item => item.id === id)?.quantity || 0;
        }

        function increaseCartQuantity(id: number) {
            setCartItems(currItems => {
                if (currItems.find(item => item.id === id) == null) {
                    return [...currItems, { id, quantity: 1}]
                } else {
                    return currItems.map(item => {
                        if (item.id === id) {
                            return {...item, quantity: item.quantity + 1 }
                        } else {
                            return item
                        }
                    })
                }
            })
        }

        function decreaseCartQuantity(id: number) {
            setCartItems(currItems => {
                if (currItems.find(item => item.id === id)?.quantity === 1 ) {
                    return currItems.filter(item => item.id !== id)
                } else {
                    return currItems.map(item => {
                        if (item.id === id) {
                            return {...item, quantity: item.quantity - 1 }
                        } else {
                            return item
                        }
                    })
                }
            })
        }

        function removeFromCart(id: number) {
            setCartItems(currItems => {
                return currItems.filter(item => item.id !== id)
            })
        }

    return (
        <RestaurantCartContext.Provider
            value={{
                getItemQuantity,
                increaseCartQuantity,
                decreaseCartQuantity,
                removeFromCart,
                cartItems,
                cartQuantity,
                setOpenBasket,
                openBasket
            }}>
            {children}
            <RestaurantCart open={openBasket} onClose={() => setOpenBasket(false)}/>
        </RestaurantCartContext.Provider>
    )
}
