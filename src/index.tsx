import React from "react";
import AuthApp from "./components/app/AuthApp";
import "./index.css";
import { createRoot } from 'react-dom/client';

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(<AuthApp/>);

