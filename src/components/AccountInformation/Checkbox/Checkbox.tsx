import React, { ReactNode } from "react";
import styles from "./Checkbox.module.css";

type CheckboxProps = {
    id: string,
    name: string,
    htmlFor: string,
    children: ReactNode
}

export const Checkbox: React.FC<CheckboxProps> = ({id, name, htmlFor, children}) => {

    return (
        <div className={styles.checkbox}>
            <input
                type="checkbox"
                id={id}
                name={name}
                defaultChecked
            />
        <label htmlFor={htmlFor}>{children}</label>
        </div>
    )
}