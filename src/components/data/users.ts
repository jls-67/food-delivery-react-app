const users = [
    {
        firstName: "John",
        lastName: "Verne",
        email: "test@test.com",
        phoneNumber: "2752552552",
        address: "120str, Baldner",
        password: "12345678"
    },
    {
        firstName: "Jane",
        lastName: "Collins",
        email: "test1@test.com",
        phoneNumber: "2752552552",
        address: "120str, Baldner",
        password: "12345678"
    },
    {
        firstName: "John",
        lastName: "Lee",
        email: "test2@test.com",
        phoneNumber: "2752552552",
        address: "120str, Baldner",
        password: "12345678"
    },
    {
        firstName: "John",
        lastName: "Verne",
        email: "test3@test.com",
        phoneNumber: "2752552552",
        address: "120str, Baldner",
        password: "12345678"
    }
]
export default users;