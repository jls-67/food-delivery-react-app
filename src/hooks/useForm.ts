import {ChangeEvent, useState} from "react";

function useForm<T = any>(initialState: T, onSubmit: (formData: any) => void) {
    const [formData, setFormData] = useState<T>(initialState);

    const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }

    const handleSubmit = (e: ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSubmit?.(formData);
    }

    return {formData, handleInputChange, handleSubmit};
}

export default useForm;
