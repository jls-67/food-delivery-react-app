import {useState} from "react";
import styles from "./Account.module.css";
import AccountInformation from "../../components/AccountInformation/AccountInformation";
import AccountBtn from "../../components/AccountBtn/AccountBtn";
import { FiUser, FiMapPin, FiCreditCard, FiShield } from "react-icons/fi";


const Account = () => {

    const [ activeSection, setActiveSection ] = useState('AccountSection');

    const accountPage = () => {
        switch(activeSection) {
          case "AccountSection":
            return <AccountInformation />;
          case "AddressSection":
            return <div>Address section</div>;
          case "PaymentsSection":
            return <div>Payment section</div>;
          case "SecuritySection":
            return <div>Security Section</div>;
          default:
            return <h1>No section found.</h1>
        }
      }

    return (
            <div className={styles.settings_main_container}>
                <div className={styles.btn_container}>
                <h3 className={`${styles.settings_heading} ${styles.heading}`}>Settings</h3>
                <AccountBtn
                    icon={<FiUser/>}
                    title={"Account"}
                    description={"Personal Information"}
                    onClick={() => setActiveSection("AccountSection")}
                />
                <AccountBtn
                    icon={<FiMapPin/>}
                    title={"Address"}
                    description={"Shippings addresses"}
                    onClick={() => setActiveSection("AddressSection")}
                />
                <AccountBtn
                    icon={<FiCreditCard/>}
                    title={"Payment methods"}
                    description={"Connected credit cards"}
                    onClick={() => setActiveSection("PaymentsSection")}
                />
                 <AccountBtn
                    icon={<FiShield/>}
                    title={"Security"}
                    description={"Password, 2FA"}
                    onClick={() => setActiveSection("SecuritySection")}
                />
                </div>
                <div>
                    {accountPage()}
                </div>
            </div>
    )
}

export default Account;
