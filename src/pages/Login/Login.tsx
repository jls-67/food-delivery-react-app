import React, {useContext} from "react";
import {Navigate} from "react-router-dom";
import LoginForm from "../../components/LoginForm/LoginForm";
import LoginReviews from "../../components/LoginPageReviews/LoginPageReviews";
import styles from "./Login.module.css";
import {AuthContext} from "../../Context/Authentication";

const Login: React.FC = () => {
    const {isLoggedIn} = useContext(AuthContext);

    if(isLoggedIn){
        return  <Navigate to="/home"/>
    }

    return (
        <div className={styles.container}>
            <LoginForm/>
            <LoginReviews/>
        </div>
    )
}

export default Login;
