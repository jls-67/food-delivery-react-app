import { useRestaurantCart } from "../../Context/RestaurantCardContext";
import BasketItem from "./BasketItem/BasketItem";
import styles from "./Basket.module.css";
import {FiXCircle} from "react-icons/fi";

const Basket = ({open, onClose}: any) => {
    const { cartItems } = useRestaurantCart();

    if (!open) return null;

    return (
        <div className={styles.basket_container}>
            <div className={styles.header}>
                <h3 className={styles.heading}>Your Basket</h3>
                <button className={styles.close_btn} onClick={onClose}><FiXCircle/></button>
            </div>
            <div className={styles.cart_container}>
                {cartItems.map(item => (
                    <BasketItem key={item.id} {...item}/>
                ))
                }
            </div>
        </div>
    )
}

export default Basket;
