import React, { useState } from "react";
import Discount from "../../components/Discount/Discount";
import RestaurantsList from "../../components/RestaurantsList/RestaurantsList";
import CategoryItems from "../../components/Categories/CategoryList";
import styles from "./Home.module.css";

const Home: React.FC = (): JSX.Element => {
    const [selectedCategory, setSelectedCategory] = useState<any>('');

    return (
        <div className={styles.main_container}>
            <Discount/>
            <CategoryItems setSelectedCategory={setSelectedCategory}/>
            <RestaurantsList selectedCategory={selectedCategory}/>
        </div>
    )
}

export default Home;