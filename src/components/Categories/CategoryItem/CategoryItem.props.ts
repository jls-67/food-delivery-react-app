export interface iCategoryListProps {
    imgPath: string,
    children: React.ReactNode,
    alt: string,
    onClick?: () => void,
    setSelectedCategory: any,
    value: string
}
