import React from "react";
import styles from "./LoginPageReviews.module.css";
import reviewImg from "./assets/reviews.png";
import squaredImg from "./assets/squared.png";
import horizontalImg from "./assets/horizontal.png";

const LoginReviews: React.FC = () => {
    return (
        <div className={styles.right_col_container}>
        <div className={styles.gallery_container}>
                    <img className={styles.reviews_img} src={reviewImg} alt="" />
                    <img className={styles.squared_img} src={squaredImg} alt="" />
                    <img className={styles.horizontal_img} src={horizontalImg} alt="" />
                </div>
                <div className={styles.reviews_container}>
                    <h3 className={styles.reviews_heading}>Leave reviews for all meals</h3>
                    <p className={styles.reviews_description}>Lorem ipsum dolor sit amet, magna scaevola his ei. Cum te paulo probatus molestiae, eirmod assentior eum ne, et omnis antiopam mel.</p>
                    <div>
                        <span className={styles.dot}></span>
                        <span className={`${styles.dot} ${styles.checked_dot}`}></span>
                        <span className={styles.dot}></span>
                        <span className={styles.dot}></span>
                    </div>
                </div>
        </div>
    )
}

export default LoginReviews;