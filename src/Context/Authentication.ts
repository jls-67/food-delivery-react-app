import {createContext} from "react";
import {userType} from "../components/app/AuthApp";

export interface ContextState {
    isLoggedIn: boolean;
    user: userType | null;
    setLoggedIn: (user: userType) => void;
    setLogOut: () => void;
    profileImage: string;
    defaultImage: string
    setProfileImage: (src: string) => void;
}

export const AuthContext = createContext({
    isLoggedIn: false,
    setLoggedIn: (user: userType) => {
    },
    setLogOut: () => {
    },
} as ContextState);



