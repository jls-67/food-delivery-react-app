import React, {useContext, useState} from "react";
import styles from './LoginForm.module.css';
import logo from "../Navbar/assets/logo.svg";
import useForm from "../../hooks/useForm";
import users from "../data/users";
import {AuthContext} from "../../Context/Authentication";
import {useNavigate} from "react-router-dom";
import Input from "../Input/Input";


const LoginForm: React.FC = () => {
    const {handleInputChange, handleSubmit} = useForm({}, onSubmit);
    const [ errorMessage, setErrorMessage ] = useState<string | null>("");
    const {setLoggedIn} = useContext(AuthContext);
    const navigate = useNavigate();

    function onSubmit(frmData: any) {
        const found = users.find((val) => {
            return val.email === frmData.email && val.password === frmData.password
        });

        if (!!found) {
            setLoggedIn({
                firstName: found.firstName,
                lastName: found.lastName,
                phoneNumber: found.phoneNumber,
                email: found.email
            });
            navigate('../')
        } else {
            return setErrorMessage("Wrong password and/or wrong email.");
        }
    }

    return (
        <div className={styles.left_col_container}>
            <div>
                <img src={logo} className={styles.logo} alt={'logo'}/>
            </div>
            <form className={styles.form_container} action="" method="post" onSubmit={handleSubmit}>
                <span className={styles.form_title}>Login</span>

                {
                    errorMessage ? <p className={styles.error_message}> {errorMessage} </p> :
                    <div>
                        <p className={styles.login_par}>Sign in with your data that you entered during your registration.</p>
                    </div>
                }

                <div className={`${styles.email} ${styles.flex_input}`}>
                    <label htmlFor="email">Email</label>
                    <Input
                        type={"email"}
                        name={"email"}
                        placeholder={"name@example.com"}
                        required={true}
                        onChange={handleInputChange}
                    />
                </div>
                <div className={`${styles.password} ${styles.flex_input}`}>
                    <label htmlFor="password">Password</label>
                    <Input
                        type={"password"}
                        name={"password"}
                        id={"password"}
                        placeholder={"min. 8 characters"}
                        required={true}
                        onChange={handleInputChange}
                    />
                </div>
                <div className={styles.logged_in}>
                    <input className={styles.checkbox} type="checkbox" name="logged_in" id="logged_in"/>
                    <label htmlFor="logged_in">Keep me logged in</label>
                </div>
                <div className={styles.login_btn}>
                    <input type="submit" value="Login"/>
                </div>
                <div className={styles.forgot_psw}>
                    <button>Forgot password</button>
                </div>
                <div className={styles.signup}>
                    <span>Don't have any account? <button>Sign up</button></span>
                </div>
            </form>
        </div>

    )
}

export default LoginForm;


