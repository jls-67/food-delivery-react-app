import { useRestaurantCart } from "../../../Context/RestaurantCardContext";
import restaurants from "../../data/restaurants";
import styles from "./BasketItem.module.css";
import {FiMinusCircle, FiPlusCircle, FiTrash2} from "react-icons/fi";

type BasketItemProps = {
    id: number,
    quantity: number
}

const BasketItem = ({id, quantity}: BasketItemProps) => {
    const {removeFromCart, increaseCartQuantity, decreaseCartQuantity} = useRestaurantCart();

    const item = restaurants.find(i => i.id === id )
    if (item == null) return null

    return (
        <div>
        <div className={styles.item_img}>
            <img src={item.img} alt=""/>
        </div>
        <div>
        <div className={styles.item_title}>
          {item.title}{" "}
          {quantity > 1 && (
            <span>
              x{quantity}
            </span>
          )}
          <div className={styles.buttons}>
            <button className={styles.minus_btn} onClick={() => decreaseCartQuantity(id)}>
                <FiMinusCircle/>
            </button>{' '}
            <button className={styles.plus_btn} onClick={() => increaseCartQuantity(id)}>
                <FiPlusCircle/>
            </button>
            <button className={styles.plus_btn} onClick={() => removeFromCart(id)}>
                <FiTrash2/>
            </button>
          </div>
        </div>
        <div className={styles.price}>{`$${item.price}`}
        </div>
      </div>
      <div className={styles.total_price}> {`Total of $${(item.price * quantity)}`}</div>
      </div>
    )
}

export default BasketItem;