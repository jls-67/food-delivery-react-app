import React, {useContext, useState} from "react";
import styles from "./Navbar.module.css";
import logo from "./assets/logo.svg";
import {FiShoppingBag} from "react-icons/fi";
import {Link, NavLink} from "react-router-dom";
import {AuthContext} from "../../Context/Authentication";
import SearchBar from "./SearchBar/SearchBar";
import { useRestaurantCart } from "../../Context/RestaurantCardContext";



const Navbar: React.FC = (): JSX.Element => {
    const { cartQuantity, setOpenBasket } = useRestaurantCart();
    const {profileImage} = useContext(AuthContext);

    return (
        <div>
            <header className={styles.header}>
                <nav className={styles.navbar}>
                    <div className={`${styles.left_navbar} ${styles.flex_row}`}>
                        <div className={styles.logo}>
                            <Link to="/home">
                                <img src={logo} alt="logo"/>
                            </Link>
                        </div>
                        <SearchBar/>
                    </div>
                    <div className={`${styles.right_navbar} ${styles.flex_row}`}>
                        <ul id="navlist" className={`${styles.primary_nav} ${styles.flex_row}`}>
                            <li><NavLink to="/home">Restaurants</NavLink></li>
                            <li><NavLink to="/deals">Deals</NavLink></li>
                            <li><NavLink to="/orders">My Orders</NavLink></li>
                        </ul>
                        <ul className={`${styles.secondary_nav} ${styles.flex_row}`}>
                            <li>
                                <span className={styles.pending_number}>{cartQuantity}</span>
                                <button className={styles.shopping_bag_icon} onClick={() => setOpenBasket(true)}><FiShoppingBag/></button>
                            </li>
                            <li>
                                <NavLink
                                    to="/account"
                                    // className={isActive =>
                                    // "account_link" + (!isActive ? " unselected" : "")}
                                    >
                                    <img src={profileImage}
                                        // src={profileImage ? profileImage : userImg}

                                        alt="user"/>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <div className={styles.horizontal_line}></div>
        </div>
    )
}

export default Navbar;