import { ReactNode, DetailedHTMLProps, ButtonHTMLAttributes } from 'react';


export interface iButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    children: ReactNode;
    background?: 'dark' | 'light';
    className?: string;
    border?: string,
    size?: string
}