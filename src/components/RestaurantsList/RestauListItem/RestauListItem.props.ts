
export default interface iRestaurantListProps {
    item: {
        id: number,
        img: string,
        alt: string,
        title: string,
        featured?: string,
        duration: string | number,
        price: number,
        categoryImg: string,
        category: string[],
    }
}