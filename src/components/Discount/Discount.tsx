import React from "react";
import styles from "./Discount.module.css";
import iceCreamImg from "./assets/ice_cream-big.png";
import burgerImg from "./assets/burger-big.png";

const Discount: React.FC = () => {
    return (
            <div className={styles.box_container}>
                <button>
                    <div className={`${styles.left_box} ${styles.flex_box}`}>
                        <div className={styles.box_image}>
                            <img src={iceCreamImg} alt="ice cream" />
                        </div>
                        <div className={styles.box_content}>
                            <span className={styles.box_heading}>All deserts</span>
                            <span className={styles.box_off}>20% OFF</span>
                            <span className={styles.box_name}>Deserty</span>
                        </div>
                    </div>
                </button>
                <button>
                    <div className={`${styles.right_box} ${styles.flex_box}`}>
                        <div className={styles.box_image}>
                            <img src={burgerImg} alt="burger" />
                        </div>
                        <div className={styles.box_content}>
                            <span className={styles.box_heading}>Big Burgers</span>
                            <span className={`${styles.box_off} ${styles.off_color}`}>50% OFF</span>
                            <span className={styles.box_name}>Fooddies</span>
                        </div>
                    </div>
                </button>
            </div>
    )
}

export default Discount;