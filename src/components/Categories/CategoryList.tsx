import React from "react";
import styles from "./CategoryList.module.css";
import CategoryListItem from "./CategoryItem/CategoryItem";
import pizzaImg from "./assets/pizza.svg";
import burgerImg from "./assets/burger.svg";
import bbqImg from "./assets/meat.svg";
import sushiImg from "./assets/sushi.svg";
import broccoliImg from "./assets/broccoli.svg";
import cakeImg from "./assets/cake.svg";
import categories from "../data/categories";

export interface iCategoryItemsProps {
    setSelectedCategory: any
}

const CategoryItems = (props: iCategoryItemsProps) => {
    const  {setSelectedCategory} = props;

    return (
        <div className={styles.category_container}>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={pizzaImg}
                alt={"pizza icon"}
                value={"Pizza"}>
                    Pizza
            </CategoryListItem>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={burgerImg}
                alt={"burger icon"}
                value={"Burger"}>
                    Burger
            </CategoryListItem>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={bbqImg}
                alt={"meat icon"}
                value={"BBQ"}>
                    BBQ
            </CategoryListItem>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={sushiImg}
                alt={"sushi icon"}
                value={"Sushi"}>
                    Sushi
            </CategoryListItem>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={broccoliImg}
                alt={"broccoli icon"}
                value={"Vegan"}>
                    Vegan
            </CategoryListItem>
            <CategoryListItem
                setSelectedCategory={setSelectedCategory}
                imgPath={cakeImg}
                alt={"cake icon"}
                value={"Desserts"}>
                    Desserts
            </CategoryListItem>
        {/* {
            categories.map((item) => {
                return (
                    <li>
                        <CategoryItems setSelectedCategory={undefined}/>
                    </li>
                )


            })
        } */}
        </div>
    )
}

export default CategoryItems;