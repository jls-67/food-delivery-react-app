const categories = [
    {
        category: "Pizza",
        image: '../../../assets/homeAssets/pizza.svg',
        alt: 'pizza'
    },
    {
        category: "Burger",
        image: '../../../assets/homeAssets/burger.svg',
        alt: 'burger'
    },
    {
        category: "BBQ",
        image: '../../../assets/homeAssets/meat.svg',
        alt: 'meat'
    },
    {
        category: "Sushi",
        image: '../../../assets/homeAssets/sushi.svg',
        alt: 'sushi'
    },
    {
        category: "Vegan",
        image: '../../../assets/homeAssets/broccoli.svg',
        alt: 'broccoli'
    },
    {
        category: "Desserts",
        image: '../../../assets/homeAssets/cake.svg',
        alt: 'cake'
    },

]

export default categories;