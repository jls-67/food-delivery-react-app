import React from "react";
import styles from "./CategoryItem.module.css";
import { iCategoryListProps } from "./CategoryItem.props";



const CategoryItem: React.FC<iCategoryListProps> = ({imgPath, children, alt, onClick, ...props}): JSX.Element => {
    const  {setSelectedCategory, value} = props;

    return (
    <div className={styles.category}>
            <button className={styles.categroy_btn} onClick={() => setSelectedCategory(value)} >
                <div className={`${styles.pizza_item} ${styles.flex_container}`}>
                    <img className={styles.category_item_img} src={imgPath} alt={alt} />
                    <span>{children}</span>
                </div>
            </button>
        </div>
    )
}

// const CategoryItem = (props: any) => {
//     const {category, alt, image} = props;
//     const  {setSelectedCategory, value} = props;
//     return (
//         <div className={styles.category}>
//              <button className={styles.categroy_btn} onClick={() => setSelectedCategory(value)} >
//                  <div className={`${styles.pizza_item} ${styles.flex_container}`}>
//                      <img className={styles.category_item_img} src={image} alt={alt} />
//                      <span>{category}</span>
//                  </div>
//              </button>
//          </div>
//     )
// }

export default CategoryItem;
