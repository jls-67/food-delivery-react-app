import styles from "./Input.module.css";
import cn from 'classnames';
import { iInputProps } from './Input.props';
import React from 'react';


const Input: React.FC<iInputProps> = ({type="checkbox", className, ...props}): JSX.Element => {
    return (
        <input
            className={cn(styles.input, {
                // [styles.checkbox]: type === 'checkbox',

            })}
            {...props}
        />
    );
}

export default Input;