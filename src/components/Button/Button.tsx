import styles from "./Button.module.css";
import cn from 'classnames';
import { iButtonProps } from './Button.props';
import React from 'react';


const Button: React.FC<iButtonProps> = ({background = 'blue', border, size, children, className, ...props}): JSX.Element => {
    return (
        <button
            className={cn(styles.btn, {
                [styles.blue]: background === 'blue',
                [styles.none]: background === 'none',
                [styles.border_pink]: border === 'pink',
                [styles.border_blue]: border === 'blue',
                [styles.border_grey]: border === 'grey',
                [styles.border_none]: border === 'none',
                [styles.big]: size === 'big',
                [styles.small]: size === 'small',
            })}
            {...props}
        >
        {children}
        </button>
    );
}

export default Button;