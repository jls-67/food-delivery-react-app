import {BrowserRouter, Routes, Route} from "react-router-dom";
import Account from "../../pages/Account";
import Home from "../../pages/Home/Home";
import Login from "../../pages/Login";
import NotFound from "../../pages/NotFound/NotFound";
import Layout from "../Layout/Layout";
import Orders from "../Navbar/Orders";
import Discount from "../Discount/Discount";
import { RestaurantCartProvider } from "../../Context/RestaurantCardContext";
// import ProtectedRoutes from "../components/Routes/ProtectedRoutes";

const App = () => {
    return (
        <BrowserRouter>
            <RestaurantCartProvider>
                <Routes>
                    <Route path="/" element={<Login/>}/>
                    <Route  element={<Layout/>}>
                        <Route path="/home" element={<Home/>}/>
                        <Route path="/account" element={<Account/>}/>
                        <Route path="/deals" element={<Discount/>}/>
                        <Route path="/orders" element={<Orders/>}/>
                        <Route path="*" element={<NotFound/>}/>
                    </Route>

                    {/* <Route path="/" element={<Layout/>}>
                        <Route path="/" element={<Home/>}/>
                        <Route element={<ProtectedRoutes />}>
                            <Route path="/account" element={<Account/>}/>
                        </Route>
                        <Route path="/deals" element={<Deals/>}/>
                        <Route path="/orders" element={<Orders/>}/>
                    </Route>
                        <Route path="/login" element={<Login/>}/>
                        <Route path="*" element={<NotFound/>}/> */}
                </Routes>
            </RestaurantCartProvider>
        </BrowserRouter>
    )
}

export default App;
