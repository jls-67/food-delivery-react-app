import App from "./App";
import {useCallback} from "react";
import {useState} from "react";
import {AuthContext, ContextState} from "../../Context/Authentication"
import userImg from "./assets/img.svg";


export type userType = {
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
}

const AuthApp = () => {

    const defaultImage = userImg;
    const [user, setUser] = useState<userType | null>({
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: ""
    });
    const [profileImage, setProfileImage] = useState<ContextState | any>(defaultImage);

    const setLoggedInFn = useCallback((user: userType) => {
        setUser(user);
    }, []);

    const setLogOut = useCallback(() => {
        setUser(null);
    }, []);

    return (
        <AuthContext.Provider value={{
            user,
            isLoggedIn: !!(user ? Object.keys(user).length : user),
            setLoggedIn: setLoggedInFn,
            setLogOut: setLogOut,
            profileImage,
            setProfileImage: setProfileImage,
            defaultImage
        }}>
            <App/>
        </AuthContext.Provider>
    )
}

export default AuthApp;
