import {useContext} from "react";
import {AuthContext} from "../../Context/Authentication";
import useForm from "../../hooks/useForm";
import { Link } from "react-router-dom";
import { Checkbox } from "./Checkbox/Checkbox";
import styles from "./AccountInformation.module.css";
import Input from "../Input/Input";
import Button from "../Button";

const AccountInformation = () => {

    const {user, profileImage, setProfileImage, defaultImage } = useContext(AuthContext);
    const {handleInputChange, handleSubmit,formData} = useForm({...user}, onSubmit)
    const { setLogOut} = useContext(AuthContext);

    function onSubmit(frmData: any) {
    }

    function handleChange(e: any) {
        setProfileImage(URL.createObjectURL(e.target.files[0]));
    }

    function handleRemoveItem () {
        setProfileImage(defaultImage);
       };


    return (

        <div className={styles.account_container}>
            <div className={`${styles.settings_heading} ${styles.heading}`}>
                <h4>Account</h4>
            </div>
            <div className={styles.account_form_container}>
                <div className={`${styles.personal_info_heading} ${styles.form_heading}`}>
                    Personal information
                </div>
                <form
                    className={styles.account_form}
                    action=""
                    method=""
                    onSubmit={handleSubmit}
                >
                    <div className={styles.account_photo_container}>
                        <label htmlFor="photo">Avatar</label>
                        <div className={styles.account_photo_settings}>
                            <input
                                className={styles.photo_btn}
                                type="image"
                                src={profileImage}
                                alt="user"
                            />
                            <label className={`${styles.change_btn} ${styles.general_btn}`}>
                                <input
                                    type="file"
                                    accept=".jpg, .jpeg, .png"
                                    onChange={handleChange}
                            />
                                Change
                            </label>
                            <input className={`${styles.remove_btn} ${styles.general_btn}`}
                                   type="button"
                                   value="Remove"
                                   onClick={handleRemoveItem}
                            />
                        </div>
                    </div>
                    <div className={styles.input_container}>
                        <div>
                            <label htmlFor="first_name">First name</label>
                            <Input
                                type={"text"}
                                name={"firstName"}
                                placeholder="Jane"
                                onChange={handleInputChange}
                                value={formData.firstName}
                            />
                        </div>
                        <div>
                            <label htmlFor="last_name">Last name</label>
                            <Input
                                type={"text"}
                                name={"lastName"}
                                placeholder="Robertson"
                                onChange={handleInputChange}
                                value={formData.lastName}
                            />
                        </div>
                        <div>
                            <label htmlFor="email">Email</label>
                            <Input
                                type={"email"}
                                name={"email"}
                                placeholder="jane.robertson@example.com"
                                onChange={handleInputChange}
                                value={formData.email}
                            />
                        </div>
                        <div>
                            <label htmlFor="phone_number">Phone number</label>
                            <Input
                                type={"tel"}
                                name={"phoneNumber"}
                                placeholder="(217) 555-0113"
                                pattern="(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}"
                                onChange={handleInputChange}
                                value={formData.phoneNumber}
                            />
                        </div>
                    </div>
                    <div className={styles.account_checkbox_container}>
                        <div className={`${styles.email_notification_heading} ${styles.form_heading}`}>
                            Email notifications
                        </div>
                        <div className={styles.checkbox_container}>
                            <Checkbox
                                id="new_deals"
                                name="new_deals"
                                htmlFor={"new_deals"}>
                            New deals
                            </Checkbox>
                            <Checkbox
                                id="new_restaurants"
                                name="new_restaurants"
                                htmlFor={"new_restaurants"}>
                            New restaurants
                            </Checkbox>
                            <Checkbox
                                id="order_statuses"
                                name="order_statuses"
                                htmlFor={"order_statuses"}>
                            Order statuses
                            </Checkbox>
                            <Checkbox
                                id="password_changes"
                                name="password_changes"
                                htmlFor={"password_changes"}>
                            Password changes
                            </Checkbox>
                            <Checkbox
                                id="special_offers"
                                name="special_offers"
                                htmlFor={"special_offers"}>
                            Special offers
                            </Checkbox>
                            <Checkbox
                                id="newsletter"
                                name="newsletter"
                                htmlFor={"newsletter"}>
                            Newsletter
                            </Checkbox>
                        </div>
                    </div>
                    <div className={styles.buttons_container}>
                        <div className={styles.log_btn}>
                            <Link to="/"><input
                                className={`${styles.logout_btn} ${styles.general_btn}`}
                                type="button"
                                name="logout"
                                value="Log out"
                                onClick={setLogOut}
                            /></Link>
                        </div>
                        <div className={styles.changes_btn}>
                            <input
                                className={`${styles.discard_btn} ${styles.general_btn}`}
                                type="button"
                                value="Discard changes"
                            />
                            <input
                                className={`${styles.save_btn} ${styles.general_btn}`}
                                type="submit"
                                value="Save changes"
                            />
                        </div>
                    </div>
                </form>
            </div>
        </div>
)
}

export default AccountInformation;


