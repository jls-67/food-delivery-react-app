import { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes } from 'react';


export interface iInputProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    type: string,
    name: string,
    id?: string,
    placeholder?: string,
    required?: boolean,
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void,
    className?: string;
}