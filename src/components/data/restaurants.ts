
const restaurants = [
    {
        id: 1,
        img: '../../assets/royal_sushi.png',
        alt: "food",
        featured: "featured",
        title: "Royal Sushi House",
        duration: "30-40 min",
        price: 32,
        categoryImg:  "../../../assets/sushi.svg",
        category: ["Sushi"]
    },
    {
        id: 2,
        img: "../../assets/french_fries.png",
        alt: "food",
        featured: "featured",
        title: "Burgers & Pizza",
        duration: "40-60 min",
        price: 24,
        categoryImg:  ["../../../assets/burger.svg", "../../../assets/pizza.svg"],
        category: ["Burger", "Pizza"]
    },
    {
        id: 3,
        img: "../../assets/ninja_sushi.png",
        alt: "food",
        title: "Ninja sushi",
        duration: "20-40 min",
        price: 40,
        categoryImg:  "../../../assets/sushi.svg",
        category: ["Sushi"]
    },
    {
        id: 4,
        img: "../../assets/sushi_master.png",
        alt: "food",
        title: "Sushi master",
        duration: "30-40 min",
        price: 49,
        categoryImg:  "../../../assets/sushi.svg",
        category: ["Sushi"]
    },
    {
        id: 5,
        img: "../../assets/japanese_sushi.png",
        alt: "food",
        title: "Japanese sushi",
        duration: "30-40 min",
        price: 104,
        categoryImg:  "../../../assets/sushi.svg",
        category: ["Sushi"]
    },
    {
        id: 6,
        img: "../../assets/kobe.png",
        alt: "food",
        title: "Kobe",
        duration: "30-40 min",
        price: 57,
        categoryImg:  "../../../assets/sushi.svg",
        category: ["Sushi"]
    }
]

export default restaurants;