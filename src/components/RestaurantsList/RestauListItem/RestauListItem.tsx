import React from "react";
import styles from "./RestauListItem.module.css";
import { FiClock, FiShoppingBag } from "react-icons/fi";
import iRestaurantListProps from "./RestauListItem.props";
import { useRestaurantCart } from "../../../Context/RestaurantCardContext";


const RestauListItem: React.FC<iRestaurantListProps> = ({item}): JSX.Element => {

    const {  id, img, alt, title, featured, duration, price, categoryImg, category } = item;
    const { getItemQuantity, increaseCartQuantity } = useRestaurantCart();
    const quantity = getItemQuantity(id);

    return (
            <div>
                <div className={styles.card_image}>
                    <img src={img} alt={alt}/>
                    <div className={styles.featured_container}>
                        <span className={styles.featured}>{featured}</span>
                    </div>
                </div>
                <div className={styles.card_content_all}>
                    <div className={styles.card_content}>
                        <span className={styles.restaurant_name}>
                            {title}
                        </span>
                        <div>
                            <button
                                className={styles.shopping_bag_icon}
                                onClick={() => increaseCartQuantity(id)}><FiShoppingBag/>
                            </button>
                            {
                                quantity !== 0 ? (<div className={styles.pending_number}>{quantity}</div>) : null
                            }
                        </div>
                    </div>
                    <div className={styles.delivery_info}>
                        <span className={styles.clock_icon}><FiClock/></span>
                        <time className={styles.delivery_duration}>{duration}</time>
                        <span className={styles.card_dot}>.</span>
                        <span className={styles.price}>{price}</span>
                    </div>
                </div>
            <div className={styles.food_type}>
                <img className={styles.food_type_img} src={categoryImg} alt="" />
                <span className={styles.food_type_title}>{category}</span>
            </div>
        </div>
    )
}

export default RestauListItem;