import styles from "./AccountBtn.module.css";

export interface iAccountBtnProps {
    icon: JSX.Element,
    title: string,
    description: string,
    onClick?: React.MouseEventHandler
}

const AccountBtn: React.FC<iAccountBtnProps> = (props): JSX.Element => {

    const { icon, title, description, onClick } = props;

    return (
                <button onClick={onClick} className={styles.account_btn}>
                    <div className={styles.settings_icon}>
                        <span>{icon}</span>
                    </div>
                    <div className={styles.settings_list_content}>
                        <span className={styles.settings_list_title}>{title}</span>
                        <span className={styles.settings_list_description}>{description}</span>
                    </div>
                </button>
    )
}

export default AccountBtn;