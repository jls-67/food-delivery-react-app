import React, {useState} from 'react';
import restaurants from '../../data/restaurants';
import styles from "./SearchBar.module.css";
import {FiSearch} from "react-icons/fi";



const SearchBar = () => {

const [searchInput, setSearchInput] = useState("");


const handleChange = (e: any) => {
    e.preventDefault();
    setSearchInput(e.target.value);
  };

  if (searchInput.length > 0) {
    restaurants.filter((item) => {
      return item.title.match(searchInput);
  });
  }

  return(
    <div className={styles.search_container}>
    <input type="text"
           role="searchbox"
           placeholder="Search"
           onChange={handleChange}
           value={searchInput}
    />
    <span className={styles.search_icon}><FiSearch /></span>
    </div>
    )};



export default SearchBar;